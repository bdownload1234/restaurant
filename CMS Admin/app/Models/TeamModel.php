<?php namespace App\Models;
use CodeIgniter\Model;
 
class TeamModel extends Model
{
    protected $table = 'team';
    protected $primaryKey = 'id';
    protected $allowedFields = ['name', 'position', 'short_description', 'biography', 'photo'];
     
    public function getTeam()
    {
        return $this->findAll();  
    }
    public function SimpanTeam($data)
    {
        $query = $this->db->table($this->table)->insert($data);
        return $query;
    }
    public function PilihTeam($id)
    {
         $query = $this->getWhere(['id' => $id]);
         return $query;
    }
    public function EditTeam($id,$data)
    {
        $query = $this->db->table($this->table)->update($data, array('id' => $id));
        return $query;
    }
    public function HapusTeam($id)
    {
        $query = $this->db->table($this->table)->delete(array('id' => $id));
        return $query;
    }
 }
