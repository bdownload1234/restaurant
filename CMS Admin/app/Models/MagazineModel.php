<?php namespace App\Models;
use CodeIgniter\Model;
 
class MagazineModel extends Model
{
    protected $table = 'magazine';
    protected $primaryKey = 'id';
    protected $allowedFields = ['title', 'pdf', 'thumbnail','created_at'];

    public function getMagazine()
    {
        return $this->findAll();  
    }
    public function SimpanMagazine($data)
    {
        $query = $this->db->table($this->table)->insert($data);
        return $query;
    }
    public function PilihMagazine($id)
    {
         $query = $this->getWhere(['id' => $id]);
         return $query;
    }
    public function EditMagazine($id,$data)
    {
        $query = $this->db->table($this->table)->update($data, array('id' => $id));
        return $query;
    }
    public function HapusMagazine($id)
    {
        $query = $this->db->table($this->table)->delete(array('id' => $id));
        return $query;
    }
 }
