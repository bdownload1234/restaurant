<?php namespace App\Models;
use CodeIgniter\Model;
 
class BannerModel extends Model
{
    protected $table = 'banner';
    protected $primaryKey = 'id';
    protected $allowedFields = ['title', 'thumbnail'];

    public function getBanner()
    {
        return $this->findAll();  
    }
    public function SimpanBanner($data)
    {
        $query = $this->db->table($this->table)->insert($data);
        return $query;
    }
    public function PilihBanner($id)
    {
         $query = $this->getWhere(['id' => $id]);
         return $query;
    }
    public function EditBanner($id,$data)
    {
        $query = $this->db->table($this->table)->update($data, array('id' => $id));
        return $query;
    }
    public function HapusBanner($id)
    {
        $query = $this->db->table($this->table)->delete(array('id' => $id));
        return $query;
    }
 }
