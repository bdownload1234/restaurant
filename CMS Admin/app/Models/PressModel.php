<?php namespace App\Models;
use CodeIgniter\Model;
 
class PressModel extends Model
{
    protected $table = 'press';
    protected $primaryKey = 'id';
    protected $allowedFields = ['title', 'content', 'thumbnail'];
     
    public function getPress()
    {
        return $this->findAll();  
    }
    public function SimpanPress($data)
    {
        $query = $this->db->table($this->table)->insert($data);
        return $query;
    }
    public function PilihPress($id)
    {
         $query = $this->getWhere(['id' => $id]);
         return $query;
    }
    public function EditPress($id,$data)
    {
        $query = $this->db->table($this->table)->update($data, array('id' => $id));
        return $query;
    }
    public function HapusPress($id)
    {
        $query = $this->db->table($this->table)->delete(array('id' => $id));
        return $query;
    }
 }
