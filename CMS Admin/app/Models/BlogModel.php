<?php namespace App\Models;
use CodeIgniter\Model;
 
class BlogModel extends Model
{
    protected $table = 'artikel';
    protected $primaryKey = 'id';
    protected $allowedFields = ['title', 'content', 'thumbnail'];
     
    public function getArtikel()
    {
        return $this->findAll();  
    }
    public function SimpanBlog($data)
    {
        $query = $this->db->table($this->table)->insert($data);
        return $query;
    }
    public function PilihBlog($id)
    {
         $query = $this->getWhere(['id' => $id]);
         return $query;
    }
    public function EditBlog($id,$data)
    {
        $query = $this->db->table($this->table)->update($data, array('id' => $id));
        return $query;
    }
    public function HapusBlog($id)
    {
        $query = $this->db->table($this->table)->delete(array('id' => $id));
        return $query;
    }
 }
