<?php namespace App\Models;
use CodeIgniter\Model;
 
class EventModel extends Model
{
    protected $table = 'event';
    protected $primaryKey = 'id';
    protected $allowedFields = ['title', 'short_description', 'date'];
     
    public function getEvent()
    {
        return $this->findAll();  
    }
    public function SimpanEvent($data)
    {
        $query = $this->db->table($this->table)->insert($data);
        return $query;
    }
    public function PilihEvent($id)
    {
         $query = $this->getWhere(['id' => $id]);
         return $query;
    }
    public function EditEvent($id,$data)
    {
        $query = $this->db->table($this->table)->update($data, array('id' => $id));
        return $query;
    }
    public function HapusEvent($id)
    {
        $query = $this->db->table($this->table)->delete(array('id' => $id));
        return $query;
    }
 }
