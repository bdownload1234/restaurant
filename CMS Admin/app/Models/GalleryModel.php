<?php namespace App\Models;
use CodeIgniter\Model;
 
class GalleryModel extends Model
{
    protected $table = 'gallery';
    protected $primaryKey = 'id';
    protected $allowedFields = ['caption', 'thumbnail'];

    public function getGallery()
    {
        return $this->findAll();  
    }
    public function SimpanGallery($data)
    {
        $query = $this->db->table($this->table)->insert($data);
        return $query;
    }
    public function PilihGallery($id)
    {
         $query = $this->getWhere(['id' => $id]);
         return $query;
    }
    public function EditGallery($id,$data)
    {
        $query = $this->db->table($this->table)->update($data, array('id' => $id));
        return $query;
    }
    public function HapusGallery($id)
    {
        $query = $this->db->table($this->table)->delete(array('id' => $id));
        return $query;
    }
 }
