<?php namespace App\Models;
use CodeIgniter\Model;
 
class BannerVideoModel extends Model
{
    protected $table = 'banner_video';
    protected $primaryKey = 'id';
    protected $allowedFields = ['title', 'video'];

    public function getBannerVideo()
    {
        return $this->findAll();  
    }
    public function SimpanBannerVideo($data)
    {
        $query = $this->db->table($this->table)->insert($data);
        return $query;
    }
    public function PilihBannerVideo($id)
    {
         $query = $this->getWhere(['id' => $id]);
         return $query;
    }
    public function EditBannerVideo($id,$data)
    {
        $query = $this->db->table($this->table)->update($data, array('id' => $id));
        return $query;
    }
    public function HapusBannerVideo($id)
    {
        $query = $this->db->table($this->table)->delete(array('id' => $id));
        return $query;
    }
 }
