<?php namespace App\Models;
use CodeIgniter\Model;
 
class ContactModel extends Model
{
    protected $table = 'contact';
    protected $primaryKey = 'id';
    protected $allowedFields = ['name', 'email', 'message'];

    public function getContact()
    {
        return $this->findAll();  
    }
    public function SimpanContact($data)
    {
        $query = $this->db->table($this->table)->insert($data);
        return $query;
    }
    public function PilihContact($id)
    {
         $query = $this->getWhere(['id' => $id]);
         return $query;
    }
    public function EditContact($id,$data)
    {
        $query = $this->db->table($this->table)->update($data, array('id' => $id));
        return $query;
    }
    public function HapusContact($id)
    {
        $query = $this->db->table($this->table)->delete(array('id' => $id));
        return $query;
    }
 }
