<?php

namespace App\Controllers;
use App\Models\TeamModel;

class Home extends BaseController
{
    public function index()
    {
        // return view('auth/login');
        //cek apakah ada session bernama isLogin
        if(!$this->session->has('isLogin')){
            return redirect()->to('/auth/login');
        }
        
        //cek role dari session
        if($this->session->get('role') != 1){
            return redirect()->to('/user');
        }
        
        $model = new TeamModel();
        if (!$this->validate([]))
        {
            $data['validation'] = $this->validator;
            $data['team'] = $model->paginate(12, 'team');
            $data['pager'] = $model->pager;
            return view('admin/index_team',$data);
        }
    }
}
