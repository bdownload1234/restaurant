<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use App\Models\BannerModel;
 
class Banner extends BaseController
{
    public function index()
    {
        $model = new BannerModel();
        if (!$this->validate([]))
        {
            $data['validation'] = $this->validator;
            $data['banner'] = $model->getBanner();
            return view('admin/index_banner',$data);
        }
    }

    public function form(){
        helper('form');
        return view('view_form');
    }

    public function view($id){
        $model = new BannerModel();
        $data['banner'] = $model->PilihBanner($id)->getRow();
        return view('view',$data);
    }

    public function simpan(){

        $model = new BannerModel();
        if ($this->request->getMethod() !== 'post') {
            return redirect()->to('admin/banner');
        }
        $validation = $this->validate([
            'file_upload' => 'uploaded[file_upload]|mime_in[file_upload,image/jpg,image/jpeg,image/gif,image/png]|max_size[file_upload,4096]'
        ]);
 
        if ($validation == FALSE) {
        $data = array(
            'title'  => $this->request->getPost('title'),
            'slug' => url_title($this->request->getPost('title'), '-', true)
        );
        } else {
            $upload = $this->request->getFile('file_upload');
            $newName = $upload->getRandomName();
            $upload->move(WRITEPATH . '../../assets/images/',$newName);
            $data = array(
                'title'  => $this->request->getPost('title'),
                'slug' => url_title($this->request->getPost('title'), '-', true),
                'thumbnail' => $newName
            );
        }
        $model->SimpanBanner($data);
        return redirect()->to('admin/banner')->with('berhasil', 'Data Berhasil di Simpan');
    }

    public function form_edit($id){
        $model = new BannerModel();
        helper('form');
        $data['artikel'] = $model->PilihBanner($id)->getRow();
        return view('form_edit',$data);
    }

    public function edit(){
        $model = new BannerModel();
        if ($this->request->getMethod() !== 'post') {
            return redirect()->to('banner');
        }
        $id = $this->request->getPost('id');
        $validation = $this->validate([
            'file_upload_edit' => 'uploaded[file_upload_edit]|mime_in[file_upload_edit,image/jpg,image/jpeg,image/gif,image/png]|max_size[file_upload_edit,4096]'
        ]);
 
        if ($validation == FALSE) {
            $data = array(
            'title'  => $this->request->getPost('title'),
            'slug' => url_title($this->request->getPost('title'), '-', true)
        );
        } else {
            $dt = $model->PilihBanner($id)->getRow();
            $gambar = $dt->thumbnail;
            $path = '../../assets/images/';
            @unlink($path.$gambar);
            $upload = $this->request->getFile('file_upload_edit');
            // $newName = $upload->getRandomName();
            $upload->move(WRITEPATH . '../../assets/images/');
            $data = array(
                'title'  => $this->request->getPost('title'),
                'slug' => url_title($this->request->getPost('title'), '-', true),
                'thumbnail' => $upload->getName()
            );
        }
        $model->EditBanner($id,$data);
        // var_dump(empty($this->request->getFile('file_upload_edit')));
        return redirect()->to('admin/banner')->with('berhasil', 'Data Berhasil di Ubah');
    }

    public function hapus($id){
        // $id = $this->request->getPost('id');
        $model = new BannerModel();
        $dt = $model->PilihBanner($id)->getRow();
        $model->HapusBanner($id);
        $gambar = $dt->thumbnail;
        $path = '../../assets/images/';
        @unlink($path.$gambar);
        return redirect()->to('admin/banner')->with('berhasil', 'Data Berhasil di Hapus');
    }

}