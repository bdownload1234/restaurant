<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use App\Models\GalleryModel;
 
class Gallery extends BaseController
{
    public function index()
    {
        $model = new GalleryModel();
        if (!$this->validate([]))
        {
            $data['validation'] = $this->validator;
            $data['gallery'] = $model->getGallery();
            return view('admin/index_gallery',$data);
        }
    }

    public function form(){
        helper('form');
        return view('view_form');
    }

    public function view($id){
        $model = new GalleryModel();
        $data['gallery'] = $model->PilihGallery($id)->getRow();
        return view('view',$data);
    }

    public function simpan(){

        $model = new GalleryModel();
        if ($this->request->getMethod() !== 'post') {
            return redirect()->to('admin/gallery');
        }
        $validation = $this->validate([
            'file_upload' => 'uploaded[file_upload]|mime_in[file_upload,image/jpg,image/jpeg,image/gif,image/png]|max_size[file_upload,4096]'
        ]);
 
        if ($validation == FALSE) {
        $data = array(
            'caption'  => $this->request->getPost('caption')
        );
        } else {
            $upload = $this->request->getFile('file_upload');
            $newName = $upload->getRandomName();
            $upload->move(WRITEPATH . '../../assets/images/',$newName);
            $data = array(
                'caption'  => $this->request->getPost('caption'),
                'thumbnail' => $newName
            );
        }
        $model->SimpanGallery($data);
        return redirect()->to('admin/gallery')->with('berhasil', 'Data Berhasil di Simpan');
    }

    public function form_edit($id){
        $model = new GalleryModel();
        helper('form');
        $data['artikel'] = $model->PilihGallery($id)->getRow();
        return view('form_edit',$data);
    }

    public function edit(){
        $model = new GalleryModel();
        if ($this->request->getMethod() !== 'post') {
            return redirect()->to('gallery');
        }
        $id = $this->request->getPost('id');
        $validation = $this->validate([
            'file_upload_edit' => 'uploaded[file_upload_edit]|mime_in[file_upload_edit,image/jpg,image/jpeg,image/gif,image/png]|max_size[file_upload_edit,4096]'
        ]);
 
        if ($validation == FALSE) {
            $data = array(
            'caption'  => $this->request->getPost('caption')
        );
        } else {
            $dt = $model->PilihGallery($id)->getRow();
            $gambar = $dt->thumbnail;
            $path = '../../assets/images/';
            @unlink($path.$gambar);
            $upload = $this->request->getFile('file_upload_edit');
            // $newName = $upload->getRandomName();
            $upload->move(WRITEPATH . '../../assets/images/');
            $data = array(
                'caption'  => $this->request->getPost('caption'),
                'thumbnail' => $upload->getName()
            );
        }
        $model->EditGallery($id,$data);
        // var_dump(empty($this->request->getFile('file_upload_edit')));
        return redirect()->to('admin/gallery')->with('berhasil', 'Data Berhasil di Ubah');
    }

    public function hapus($id){
        // $id = $this->request->getPost('id');
        $model = new GalleryModel();
        $dt = $model->PilihGallery($id)->getRow();
        $model->HapusGallery($id);
        $gambar = $dt->thumbnail;
        $path = '../../assets/images/';
        @unlink($path.$gambar);
        return redirect()->to('admin/gallery')->with('berhasil', 'Data Berhasil di Hapus');
    }

}