<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use App\Models\EventModel;
 
class Event extends BaseController
{
    public function index()
    {
        $model = new EventModel();
        if (!$this->validate([]))
        {
            $data['validation'] = $this->validator;
            $data['event'] = $model->getEvent();
            return view('admin/index_event',$data);
        }
    }

    public function form(){
        helper('form');
        return view('view_form');
    }

    public function view($id){
        $model = new EventModel();
        $data['artikel'] = $model->PilihEvent($id)->getRow();
        return view('view',$data);
    }

    public function simpan(){

        $model = new EventModel();
        if ($this->request->getMethod() !== 'post') {
            return redirect()->to('Event');
        }
        $validation = $this->validate([
            'file_upload' => 'uploaded[file_upload]|mime_in[file_upload,image/jpg,image/jpeg,image/gif,image/png]|max_size[file_upload,4096]'
        ]);
 
        if ($validation == FALSE) {
        $data = array(
            'title'  => $this->request->getPost('title'),
            'date'  => $this->request->getPost('date'),
            'time_from'  => $this->request->getPost('time_from'),
            'time_to'  => $this->request->getPost('time_to'),
            'short_description' => $this->request->getPost('short_description')
        );
        } else {
            
            $upload = $this->request->getFile('file_upload');
            $newName = $upload->getRandomName();
            $upload->move(WRITEPATH . '../public/assets/images/',$newName);
            $data = array(
                'title'  => $this->request->getPost('title'),
                'date'  => $this->request->getPost('date'),
                'time_from'  => $this->request->getPost('time_from'),
                'time_to'  => $this->request->getPost('time_to'),
                'short_description' => $this->request->getPost('short_description'),
                'photo' => $newName
            );
        }
        $model->SimpanEvent($data);
        return redirect()->to('admin/event')->with('berhasil', 'Data Berhasil di Simpan');
    }

    public function form_edit($id){
        $model = new EventModel();
        helper('form');
        $data['artikel'] = $model->PilihEvent($id)->getRow();
        return view('form_edit',$data);
    }

    public function edit(){
        $model = new EventModel();
        if ($this->request->getMethod() !== 'post') {
            return redirect()->to('/admin/event');
        }
        $id = $this->request->getPost('id');
        $validation = $this->validate([
            'file_upload_edit' => 'uploaded[file_upload_edit]|mime_in[file_upload_edit,image/jpg,image/jpeg,image/gif,image/png]|max_size[file_upload_edit,4096]'
        ]);
 
        if ($validation == FALSE) {
        $data = array(
            'title'  => $this->request->getPost('title'),
            'date'  => $this->request->getPost('date'),
            'time_from'  => $this->request->getPost('time_from'),
            'time_to'  => $this->request->getPost('time_to'),
            'short_description' => $this->request->getPost('short_description')
        );
        } else {
        $dt = $model->PilihEvent($id)->getRow();
        $gambar = $dt->photo;
        $path = '../public/assets/images/';
        @unlink($path.$gambar);
            $upload = $this->request->getFile('file_upload_edit');
            $upload->move(WRITEPATH . '../public/assets/images/');
        $data = array(
            'title'  => $this->request->getPost('title'),
            'date'  => $this->request->getPost('date'),
            'time_from'  => $this->request->getPost('time_from'),
            'time_to'  => $this->request->getPost('time_to'),
            'short_description' => $this->request->getPost('short_description'),
            'photo' => $upload->getName()
        );
        }
        $model->EditEvent($id,$data);
        return redirect()->to('/admin/event')->with('berhasil', 'Data Berhasil di Ubah');
        
    }

    public function hapus($id){
        $model = new EventModel();
        $model->HapusEvent($id);
        return redirect()->to('/admin/event')->with('berhasil', 'Data Berhasil di Hapus');
    }

}