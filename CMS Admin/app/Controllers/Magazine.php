<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use App\Models\MagazineModel;
 
class Magazine extends BaseController
{
    public function index()
    {
        $model = new MagazineModel();
        if (!$this->validate([]))
        {
            $data['validation'] = $this->validator;
            $data['magazine'] = $model->getMagazine();
            return view('admin/index_magazine',$data);
        }
    }

    public function form(){
        helper('form');
        return view('view_form');
    }

    public function view($id){
        $model = new MagazineModel();
        $data['magazine'] = $model->PilihMagazine($id)->getRow();
        return view('view',$data);
    }

    public function simpan(){

        $model = new MagazineModel();
        if ($this->request->getMethod() !== 'post') {
            return redirect()->to('admin/magazine');
        }
        $validation = $this->validate([
            'file_upload' => 'uploaded[file_upload]|mime_in[file_upload,application/pdf]',
            'file_upload_thumbnail' => 'uploaded[file_upload_thumbnail]|mime_in[file_upload_thumbnail,image/jpg,image/jpeg,image/gif,image/png]'
        ]);
 
        if ($validation == FALSE) {
        $data = array(
            'title'  => $this->request->getPost('title'),
            'slug' => url_title($this->request->getPost('title'), '-', true)
        );
        } else {
            $upload = $this->request->getFile('file_upload');
            $upload_thumbnail = $this->request->getFile('file_upload_thumbnail');
            $newName = $upload->getRandomName();
            $newName_thumbnail = $upload_thumbnail->getRandomName();
            $upload->move(WRITEPATH . '../../assets/pdf/',$newName);
            $upload_thumbnail->move(WRITEPATH . '../../assets/images/',$newName_thumbnail);
            $data = array(
                'title'  => $this->request->getPost('title'),
                'slug' => url_title($this->request->getPost('title'), '-', true),
                'pdf' => $newName,
                'thumbnail' => $newName_thumbnail,
            );
        }
        $model->SimpanMagazine($data);
        // var_dump($this->request->getFile('file_upload'));
        return redirect()->to('admin/magazine')->with('berhasil', 'Data Berhasil di Simpan');
    }

    public function form_edit($id){
        $model = new MagazineModel();
        helper('form');
        $data['artikel'] = $model->PilihMagazine($id)->getRow();
        return view('form_edit',$data);
    }

    public function edit(){
        $model = new MagazineModel();
        if ($this->request->getMethod() !== 'post') {
            return redirect()->to('magazine');
        }
        $id = $this->request->getPost('id');
        $validation = $this->validate([
            'file_upload_thumbnail_edit' => 'uploaded[file_upload_thumbnail_edit]|mime_in[file_upload_thumbnail_edit,application/pdf]',
            'file_upload_thumbnail' => 'uploaded[file_upload_thumbnail]|mime_in[file_upload_thumbnail,image/jpg,image/jpeg,image/gif,image/png]'
        ]);
 
        if ($validation == FALSE) {
            $data = array(
            'title'  => $this->request->getPost('title'),
            'slug' => url_title($this->request->getPost('title'), '-', true)
        );
        } else {
            $dt = $model->PilihMagazine($id)->getRow();
            $pdf = $dt->pdf;
            $thumbnail = $dt->thumbnail;
            $path = '../../assets/pdf/';
            $path_thumbnail = '../../assets/images/';
            @unlink($path.$pdf);
            @unlink($path_thumbnail.$thumbnail);
            $upload = $this->request->getFile('file_upload_thumbnail_edit');
            $upload_thumbnail = $this->request->getFile('file_upload_thumbnail_edit');
            // $newName = $upload->getRandomName();
            $upload->move(WRITEPATH . '../../assets/pdf/');
            $upload_thumbnail->move(WRITEPATH . '../../assets/images/');
            $data = array(
                'title'  => $this->request->getPost('title'),
                'slug' => url_title($this->request->getPost('title'), '-', true),
                'pdf' => $upload->getName(),
                'thumbnail' => $upload_thumbnail->getName(),
            );
        }
        $model->EditMagazine($id,$data);
        // var_dump(empty($this->request->getFile('file_upload_thumbnail_edit')));
        return redirect()->to('admin/magazine')->with('berhasil', 'Data Berhasil di Ubah');
    }

    public function hapus($id){
        // $id = $this->request->getPost('id');
        $model = new MagazineModel();
        $dt = $model->PilihMagazine($id)->getRow();
        $model->HapusMagazine($id);
        $pdf = $dt->pdf;
        $thumbnail = $dt->thumbnail;
        $path = '../../assets/pdf/';
        $path_thumbnail = '../../assets/images/';
        @unlink($path.$pdf);
        @unlink($path_thumbnail.$thumbnail);
        return redirect()->to('admin/magazine')->with('berhasil', 'Data Berhasil di Hapus');
    }

}