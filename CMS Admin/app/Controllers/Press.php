<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use App\Models\PressModel;
 
class Press extends BaseController
{
    public function index()
    {
        $model = new PressModel();
        if (!$this->validate([]))
        {
            $data['validation'] = $this->validator;
            $data['press'] = $model->getPress();
            return view('admin/index_press',$data);
        }
    }

    public function form(){
        helper('form');
        return view('view_form');
    }

    public function view($id){
        $model = new PressModel();
        $data['press'] = $model->PilihPress($id)->getRow();
        return view('admin/show_press',$data);
    }

    public function simpan(){

        $model = new PressModel();
        if ($this->request->getMethod() !== 'post') {
            return redirect()->to('press');
        }
        $validation = $this->validate([
            'file_upload' => 'uploaded[file_upload]|mime_in[file_upload,image/jpg,image/jpeg,image/gif,image/png]|max_size[file_upload,4096]'
        ]);
 
        if ($validation == FALSE) {
        $data = array(
            'title'  => $this->request->getPost('title'),
            'content' => $this->request->getPost('content'),
            'author' => $this->request->getPost('author'),
            'meta_description' => $this->request->getPost('meta-description'),
            'slug' => url_title($this->request->getPost('title'), '-', true)
        );
        } else {
            
            $upload = $this->request->getFile('file_upload');
            $newName = $upload->getRandomName();
            $upload->move(WRITEPATH . '../../assets/images/',$newName);
            $data = array(
                'title'  => $this->request->getPost('title'),
                'content' => $this->request->getPost('content'),
                'author' => $this->request->getPost('author'),
                'meta_description' => $this->request->getPost('meta-description'),
                'slug' => url_title($this->request->getPost('title'), '-', true),
                'thumbnail' => $newName
            );
        }
        $model->SimpanPress($data);
        return redirect()->to('admin/press')->with('berhasil', 'Data Berhasil di Simpan');
    }

    public function form_edit($id){
        $model = new PressModel();
        helper('form');
        $data['press'] = $model->PilihPress($id)->getRow();
        return view('form_edit',$data);
    }

    public function edit(){
        $model = new PressModel();
        if ($this->request->getMethod() !== 'post') {
            return redirect()->to('press');
        }
        $id = $this->request->getPost('id');
        $validation = $this->validate([
            'file_upload_edit' => 'uploaded[file_upload_edit]|mime_in[file_upload_edit,image/jpg,image/jpeg,image/gif,image/png]|max_size[file_upload_edit,4096]'
        ]);
 
        if ($validation == FALSE) {
        $data = array(
            'title'  => $this->request->getPost('title'),
            'content' => $this->request->getPost('content'),
            'meta_description' => $this->request->getPost('meta-description'),
        );
        } else {
        $dt = $model->PilihPress($id)->getRow();
        $gambar = $dt->thumbnail;
        $path = '../../assets/images/';
        @unlink($path.$gambar);
            $upload = $this->request->getFile('file_upload_edit');
            $upload->move(WRITEPATH . '../../assets/images/');
        $data = array(
            'title'  => $this->request->getPost('title'),
            'content' => $this->request->getPost('content'),
            'meta_description' => $this->request->getPost('meta-description'),
            'thumbnail' => $upload->getName()
        );
        }
        $model->EditPress($id,$data);
        return redirect()->to('admin/press')->with('berhasil', 'Data Berhasil di Ubah');
        
    }

    public function hapus($id){
        $model = new PressModel();
        $dt = $model->PilihPress($id)->getRow();
        $model->HapusPress($id);
        $gambar = $dt->thumbnail;
        $path = '../../assets/images/';
        @unlink($path.$gambar);
        return redirect()->to('admin/press')->with('berhasil', 'Data Berhasil di Hapus');
    }

}