<?= $this->extend('layout/page_layout') ?>

<?= $this->section('content') ?>
                <div class="container-fluid">
                    <div class="layout-specing">
                        <div class="d-md-flex justify-content-between">
                            <div>
                                <h5 class="mb-0">Banners</h5>

                                <nav aria-label="breadcrumb" class="d-inline-block mt-1">
                                    <ul class="breadcrumb breadcrumb-muted bg-transparent rounded mb-0 p-0">
                                        <li class="breadcrumb-item text-capitalize"><a href=<?= base_url("/admin/")?>>Home</a></li>
                                        <li class="breadcrumb-item text-capitalize active" aria-current="page">Banners</li>
                                    </ul>
                                </nav>
                            </div>

                            <div class="mt-4 mt-sm-0">
                                <a href="#" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#newblogadd">Add Banner</a>
                            </div>
                        </div>
                    
                        <div class="row">
                            <?php foreach($banner as $row):?>

                            <div class="col-xl-3 col-lg-4 col-md-6 mt-4">
                                <div class="card blog blog-primary rounded border-0 shadow overflow-hidden">
                                    <div class="position-relative">
                                        <?php
                                            if (!empty($row["thumbnail"])) {
                                                echo '<img src="'.base_url("assets/images/$row[thumbnail]").'" class="card-img-top img-fluid" alt="...">';
                                            }else{
                                                echo '<img src="'.base_url("assets/images/blog/no-image.jpg").'" class="card-img-top img-fluid" alt="...">';
                                            }
                                        ?>
                                        <div class="overlay rounded-top"></div>
                                    </div>
                                    <div class="card-body content">
                                        <h5><a href="javascript:void(0)" class="card-title title text-dark"><?=$row['title'];?></a></h5>
                                        <div class="post-meta d-flex justify-content-between mt-3">
                                            <!-- <ul class="list-unstyled mb-0">
                                                <li class="list-inline-item me-2 mb-0"><a href="javascript:void(0)" class="text-muted like"><i class="uil uil-heart me-1"></i>33</a></li>
                                                <li class="list-inline-item"><a href="javascript:void(0)" class="text-muted comments"><i class="uil uil-comment me-1"></i>08</a></li>
                                            </ul> -->
                                            <!-- <a href="#" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#newblogadd">Add Banner</a> -->
                                            <?php
                                                if (!empty($row["thumbnail"])) {
                                                    echo '<a href="#" class="text-muted readmore" data-bs-toggle="modal" data-bs-target="#editbanner" data-title="'.$row['title'].'" data-id="'.$row['id'].'" data-thumbnail="'.base_url("assets/images/$row[thumbnail]").'">Edit <i class="uil uil-angle-right-b align-middle"></i></a>';
                                                }else{
                                                    echo '<a href="#" class="text-muted readmore" data-bs-toggle="modal" data-bs-target="#editbanner" data-title="'.$row['title'].'" data-id="'.$row['id'].'" data-thumbnail="'.base_url("assets/images/blog/no-image.jpg").'">Edit <i class="uil uil-angle-right-b align-middle"></i></a>';
                                                }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="author">
                                        <!-- <small class="text-white user d-block"><i class="uil uil-user"></i> Calvin Carlo</small> -->
                                        <small class="text-white date"><i class="uil uil-calendar-alt"></i> <?=$row['created_at'];?></small>
                                    </div>
                                </div>
                            </div><!--end col-->

                            <?php endforeach;?>
                            
                            <!-- <div class="col-xl-3 col-lg-4 col-md-6 mt-4">
                                <div class="card blog blog-primary rounded border-0 shadow overflow-hidden">
                                    <div class="position-relative">
                                        <img src="<?php echo base_url('assets/images/blog/no-image.jpg'); ?>" class="card-img-top" alt="...">
                                        <div class="overlay rounded-top"></div>
                                    </div>
                                    <div class="card-body content">
                                        <h5><a href="javascript:void(0)" class="card-title title text-dark">How apps is changing the IT world</a></h5>
                                        <div class="post-meta d-flex justify-content-between mt-3">
                                            <ul class="list-unstyled mb-0">
                                                <li class="list-inline-item me-2 mb-0"><a href="javascript:void(0)" class="text-muted like"><i class="uil uil-heart me-1"></i>33</a></li>
                                                <li class="list-inline-item"><a href="javascript:void(0)" class="text-muted comments"><i class="uil uil-comment me-1"></i>08</a></li>
                                            </ul>
                                            <a href="blog-detail.html" class="text-muted readmore">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                                        </div>
                                    </div>
                                    <div class="author">
                                        <small class="text-white user d-block"><i class="uil uil-user"></i> Calvin Carlo</small>
                                        <small class="text-white date"><i class="uil uil-calendar-alt"></i> 25th June 2021</small>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-xl-3 col-lg-4 col-md-6 mt-4">
                                <div class="card blog blog-primary rounded border-0 shadow overflow-hidden">
                                    <div class="position-relative">
                                        <img src="<?php echo base_url('assets/images/blog/no-image.jpg'); ?>" class="card-img-top" alt="...">
                                        <div class="overlay rounded-top"></div>
                                    </div>
                                    <div class="card-body content">
                                        <h5><a href="javascript:void(0)" class="card-title title text-dark">Smartest Applications for Business</a></h5>
                                        <div class="post-meta d-flex justify-content-between mt-3">
                                            <ul class="list-unstyled mb-0">
                                                <li class="list-inline-item me-2 mb-0"><a href="javascript:void(0)" class="text-muted like"><i class="uil uil-heart me-1"></i>33</a></li>
                                                <li class="list-inline-item"><a href="javascript:void(0)" class="text-muted comments"><i class="uil uil-comment me-1"></i>08</a></li>
                                            </ul>
                                            <a href="blog-detail.html" class="text-muted readmore">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                                        </div>
                                    </div>
                                    <div class="author">
                                        <small class="text-white user d-block"><i class="uil uil-user"></i> Calvin Carlo</small>
                                        <small class="text-white date"><i class="uil uil-calendar-alt"></i> 25th June 2021</small>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-xl-3 col-lg-4 col-md-6 mt-4">
                                <div class="card blog blog-primary rounded border-0 shadow overflow-hidden">
                                    <div class="position-relative">
                                        <img src="<?php echo base_url('assets/images/blog/no-image.jpg'); ?>" class="card-img-top" alt="...">
                                        <div class="overlay rounded-top"></div>
                                    </div>
                                    <div class="card-body content">
                                        <h5><a href="javascript:void(0)" class="card-title title text-dark">How apps is changing the IT world</a></h5>
                                        <div class="post-meta d-flex justify-content-between mt-3">
                                            <ul class="list-unstyled mb-0">
                                                <li class="list-inline-item me-2 mb-0"><a href="javascript:void(0)" class="text-muted like"><i class="uil uil-heart me-1"></i>33</a></li>
                                                <li class="list-inline-item"><a href="javascript:void(0)" class="text-muted comments"><i class="uil uil-comment me-1"></i>08</a></li>
                                            </ul>
                                            <a href="blog-detail.html" class="text-muted readmore">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                                        </div>
                                    </div>
                                    <div class="author">
                                        <small class="text-white user d-block"><i class="uil uil-user"></i> Calvin Carlo</small>
                                        <small class="text-white date"><i class="uil uil-calendar-alt"></i> 25th June 2021</small>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-xl-3 col-lg-4 col-md-6 mt-4">
                                <div class="card blog blog-primary rounded border-0 shadow overflow-hidden">
                                    <div class="position-relative">
                                        <img src="<?php echo base_url('assets/images/blog/no-image.jpg'); ?>" class="card-img-top" alt="...">
                                        <div class="overlay rounded-top"></div>
                                    </div>
                                    <div class="card-body content">
                                        <h5><a href="javascript:void(0)" class="card-title title text-dark">Design your apps in your own way</a></h5>
                                        <div class="post-meta d-flex justify-content-between mt-3">
                                            <ul class="list-unstyled mb-0">
                                                <li class="list-inline-item me-2 mb-0"><a href="javascript:void(0)" class="text-muted like"><i class="uil uil-heart me-1"></i>33</a></li>
                                                <li class="list-inline-item"><a href="javascript:void(0)" class="text-muted comments"><i class="uil uil-comment me-1"></i>08</a></li>
                                            </ul>
                                            <a href="blog-detail.html" class="text-muted readmore">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                                        </div>
                                    </div>
                                    <div class="author">
                                        <small class="text-white user d-block"><i class="uil uil-user"></i> Calvin Carlo</small>
                                        <small class="text-white date"><i class="uil uil-calendar-alt"></i> 25th June 2021</small>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-xl-3 col-lg-4 col-md-6 mt-4">
                                <div class="card blog blog-primary rounded border-0 shadow overflow-hidden">
                                    <div class="position-relative">
                                        <img src="<?php echo base_url('assets/images/blog/no-image.jpg'); ?>" class="card-img-top" alt="...">
                                        <div class="overlay rounded-top"></div>
                                    </div>
                                    <div class="card-body content">
                                        <h5><a href="javascript:void(0)" class="card-title title text-dark">Smartest Applications for Business</a></h5>
                                        <div class="post-meta d-flex justify-content-between mt-3">
                                            <ul class="list-unstyled mb-0">
                                                <li class="list-inline-item me-2 mb-0"><a href="javascript:void(0)" class="text-muted like"><i class="uil uil-heart me-1"></i>33</a></li>
                                                <li class="list-inline-item"><a href="javascript:void(0)" class="text-muted comments"><i class="uil uil-comment me-1"></i>08</a></li>
                                            </ul>
                                            <a href="blog-detail.html" class="text-muted readmore">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                                        </div>
                                    </div>
                                    <div class="author">
                                        <small class="text-white user d-block"><i class="uil uil-user"></i> Calvin Carlo</small>
                                        <small class="text-white date"><i class="uil uil-calendar-alt"></i> 25th June 2021</small>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-xl-3 col-lg-4 col-md-6 mt-4">
                                <div class="card blog blog-primary rounded border-0 shadow overflow-hidden">
                                    <div class="position-relative">
                                        <img src="<?php echo base_url('assets/images/blog/no-image.jpg'); ?>" class="card-img-top" alt="...">
                                        <div class="overlay rounded-top"></div>
                                    </div>
                                    <div class="card-body content">
                                        <h5><a href="javascript:void(0)" class="card-title title text-dark">Design your apps in your own way</a></h5>
                                        <div class="post-meta d-flex justify-content-between mt-3">
                                            <ul class="list-unstyled mb-0">
                                                <li class="list-inline-item me-2 mb-0"><a href="javascript:void(0)" class="text-muted like"><i class="uil uil-heart me-1"></i>33</a></li>
                                                <li class="list-inline-item"><a href="javascript:void(0)" class="text-muted comments"><i class="uil uil-comment me-1"></i>08</a></li>
                                            </ul>
                                            <a href="blog-detail.html" class="text-muted readmore">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                                        </div>
                                    </div>
                                    <div class="author">
                                        <small class="text-white user d-block"><i class="uil uil-user"></i> Calvin Carlo</small>
                                        <small class="text-white date"><i class="uil uil-calendar-alt"></i> 25th June 2021</small>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-xl-3 col-lg-4 col-md-6 mt-4">
                                <div class="card blog blog-primary rounded border-0 shadow overflow-hidden">
                                    <div class="position-relative">
                                        <img src="<?php echo base_url('assets/images/blog/no-image.jpg'); ?>" class="card-img-top" alt="...">
                                        <div class="overlay rounded-top"></div>
                                    </div>
                                    <div class="card-body content">
                                        <h5><a href="javascript:void(0)" class="card-title title text-dark">Smartest Applications for Business</a></h5>
                                        <div class="post-meta d-flex justify-content-between mt-3">
                                            <ul class="list-unstyled mb-0">
                                                <li class="list-inline-item me-2 mb-0"><a href="javascript:void(0)" class="text-muted like"><i class="uil uil-heart me-1"></i>33</a></li>
                                                <li class="list-inline-item"><a href="javascript:void(0)" class="text-muted comments"><i class="uil uil-comment me-1"></i>08</a></li>
                                            </ul>
                                            <a href="blog-detail.html" class="text-muted readmore">Read More <i class="uil uil-angle-right-b align-middle"></i></a>
                                        </div>
                                    </div>
                                    <div class="author">
                                        <small class="text-white user d-block"><i class="uil uil-user"></i> Calvin Carlo</small>
                                        <small class="text-white date"><i class="uil uil-calendar-alt"></i> 25th June 2021</small>
                                    </div>
                                </div>
                            </div> -->
                        </div><!--end row-->

                        <div class="row">
                            <!-- PAGINATION START -->
                            <?= $pager->links('artikel','pagination') ?>
                            <!-- <div class="col-12 mt-4">
                                <ul class="pagination justify-content-center mb-0">
                                    <li class="page-item"><a class="page-link" href="javascript:void(0)" aria-label="Previous">Prev</a></li>
                                    <li class="page-item active"><a class="page-link" href="javascript:void(0)">1</a></li>
                                    <li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
                                    <li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
                                    <li class="page-item"><a class="page-link" href="javascript:void(0)" aria-label="Next">Next</a></li>
                                </ul>
                            </div>end col -->
                            <!-- PAGINATION END -->
                        </div><!--end row-->
                    </div>
                </div><!--end container-->

                <!-- Start Modal -->
                <div class="modal fade" id="newblogadd" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header border-bottom p-3">
                                <h5 class="modal-title" id="exampleModalLabel">Add Banner</h5>
                                <button type="button" class="btn btn-icon btn-close" data-bs-dismiss="modal" id="close-modal"><i class="uil uil-times fs-4 text-dark"></i></button>
                            </div>

                            <div class="modal-body p-3 pt-4">
                                <div class="row">
                                    <!-- <div class="col-md-4">
                                        <div class="d-grid">
                                            <p class="text-muted">Upload your blog thumbnail here, Please click "Upload Image" Button.</p>
                                            <div class="preview-box d-block justify-content-center rounded shadow overflow-hidden bg-light p-1"></div>
                                            <input type="file" id="input-file" name="input-file" accept="image/*" onchange={handleChange()} hidden />
                                            <label class="btn-upload btn btn-primary mt-4" for="input-file">Upload Image</label>
                                        </div>
                                    </div> -->
                                    <form method="post" action=<?=base_url("/banner/simpan")?> enctype="multipart/form-data">
                                    <div class="col-md-12 mt-4 mt-sm-0">
                                        <div>
                                            <!-- <form method="post" action="/blog/simpan"> -->
                                                <div class="row">

                                                    <div class="col-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Banner Title <span class="text-danger">*</span></label>
                                                            <input name="title" id="name" type="text" class="form-control" placeholder="Title :">
                                                        </div>
                                                    </div><!--end col-->
            
                                                    <!-- <div class="col-md-6">
                                                        <div class="mb-3">
                                                            <label class="form-label"> Date : </label>
                                                            <input name="date" type="text" class="form-control" id="date" value="09 January 2021">
                                                        </div>
                                                    </div>
            
                                                    <div class="col-md-6">
                                                        <div class="mb-3">
                                                            <label class="form-label"> Time to read : </label>
                                                            <input name="time" type="text" class="form-control" id="time" value="5 min read">
                                                        </div>
                                                    </div>
            
                                                    <div class="col-md-6">
                                                        <div class="mb-3">
                                                            <label class="form-label">Tag</label>
                                                            <select class="form-control">
                                                                <option value="BU">Business</option>
                                                                <option value="AG">Agency</option>
                                                                <option value="FA">Fashion</option>
                                                                <option value="SE">Selling</option>
                                                                <option value="FI">Finance</option>
                                                            </select>
                                                        </div>
                                                    </div> -->
            
                                                    <!-- <div class="col-lg-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Content <span class="text-danger">*</span></label>
                                                            <textarea name="content" id="summernote" rows="4" placeholder="Content :"></textarea>
                                                        </div>
                                                    </div> -->

                                                    <div class="col-12">
                                                        <div class="mb-3">
                                                            <p class="text-muted">Upload your banner thumbnail here, Please click "Upload Image" Button.</p>
                                                            <div class="preview-box d-block justify-content-center rounded shadow overflow-hidden bg-light p-1"></div>
                                                            <input type="file" id="input-file" name="file_upload" accept="image/*" onchange={handleChange()} hidden />
                                                            <label class="btn-upload btn btn-primary mt-4" for="input-file">Upload Image</label>
                                                        </div>
                                                    </div><!--end col-->
            
                                                    <div class="col-lg-12 text-end">
                                                        <button type="submit" class="btn btn-primary">Add Banner</button>
                                                    </div><!--end col-->
                                                </div>
                                            <!-- </form> -->
                                        </div>
                                    </div><!--end col-->
                                    </form>
                                </div><!--end row-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End modal -->

                <!-- Start Modal Edit -->
                <div class="modal fade" id="editbanner" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header border-bottom p-3">
                                <h5 class="modal-title" id="exampleModalLabel">Edit Banner</h5>
                                <button type="button" class="btn btn-icon btn-close" data-bs-dismiss="modal" id="close-modal"><i class="uil uil-times fs-4 text-dark"></i></button>
                            </div>

                            <div class="modal-body p-3 pt-4">
                                <div class="row">
                                    <!-- <div class="col-md-4">
                                        <div class="d-grid">
                                            <p class="text-muted">Upload your blog thumbnail here, Please click "Upload Image" Button.</p>
                                            <div class="preview-box d-block justify-content-center rounded shadow overflow-hidden bg-light p-1"></div>
                                            <input type="file" id="input-file" name="input-file" accept="image/*" onchange={handleChange()} hidden />
                                            <label class="btn-upload btn btn-primary mt-4" for="input-file">Upload Image</label>
                                        </div>
                                    </div> -->
                                    <form method="post" action=<?=base_url("/banner/edit")?> enctype="multipart/form-data">
                                    <div class="col-md-12 mt-4 mt-sm-0">
                                        <div>
                                            <!-- <form method="post" action="/blog/simpan"> -->
                                                <div class="row">

                                                    <div class="col-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Banner Title <span class="text-danger">*</span></label>
                                                            <input name="title" id="bannertitle" type="text" class="form-control" placeholder="Title :">
                                                        </div>
                                                    </div><!--end col-->
            
                                                    <!-- <div class="col-md-6">
                                                        <div class="mb-3">
                                                            <label class="form-label"> Date : </label>
                                                            <input name="date" type="text" class="form-control" id="date" value="09 January 2021">
                                                        </div>
                                                    </div>
            
                                                    <div class="col-md-6">
                                                        <div class="mb-3">
                                                            <label class="form-label"> Time to read : </label>
                                                            <input name="time" type="text" class="form-control" id="time" value="5 min read">
                                                        </div>
                                                    </div>
            
                                                    <div class="col-md-6">
                                                        <div class="mb-3">
                                                            <label class="form-label">Tag</label>
                                                            <select class="form-control">
                                                                <option value="BU">Business</option>
                                                                <option value="AG">Agency</option>
                                                                <option value="FA">Fashion</option>
                                                                <option value="SE">Selling</option>
                                                                <option value="FI">Finance</option>
                                                            </select>
                                                        </div>
                                                    </div> -->
            
                                                    <!-- <div class="col-lg-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Content <span class="text-danger">*</span></label>
                                                            <textarea name="content" id="summernote" rows="4" placeholder="Content :"></textarea>
                                                        </div>
                                                    </div> -->

                                                    <div class="col-12">
                                                        <div class="mb-3">
                                                            <p class="text-muted">Upload your banner thumbnail here, Please click "Upload Image" Button.</p>
                                                            <div class="preview-box2 d-block justify-content-center rounded shadow overflow-hidden bg-light p-1">
                                                                <img class="preview-content img-fluid" src="" />
                                                            </div>
                                                            <input type="file" id="input-file-edit" name="file_upload_edit" accept="image/*" onchange={handleChange()} hidden />
                                                            <label class="btn-upload btn btn-primary mt-4" for="input-file-edit">Upload Image</label>
                                                        </div>
                                                    </div><!--end col-->
            
                                                    <div class="col-lg-12 text-end">
                                                        <input name="id" id="bannerid" type="hidden">
                                                        <a href="" class="button-delete btn btn-danger">Delete Banner</a>
                                                        <button type="submit" class="btn btn-primary">Edit Banner</button>
                                                    </div><!--end col-->
                                                </div>
                                            <!-- </form> -->
                                        </div>
                                    </div><!--end col-->
                                    </form>
                                </div><!--end row-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End modal Edit-->

                <style>
                    .note-editable {
                        background-color: #FFFFFF !important;
                    }
                    .card-img-top {
                        width: 100%;
                        height: 15vw;
                        object-fit: cover;
                    }
                </style>

                <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
                <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
                <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

                <script>
                    $('#summernote').summernote({
                        placeholder: 'Content',
                        height: 300
                    });

                    $('#editbanner').on('show.bs.modal', function (event) {
                        var button = $(event.relatedTarget) // Button that triggered the modal
                        var recipient = button.data('title') // Extract info from data-* attributes
                        var thumbnail = button.data('thumbnail')
                        var id = button.data('id')
                        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                        var modal = $(this)
                        modal.find('.modal-title').text('Edit ' + recipient)
                        modal.find('.modal-body #bannertitle').val(recipient)
                        modal.find('.modal-body #bannerid').val(id)
                        modal.find('a.button-delete').attr('href', '<?=base_url("/banner/hapus/")?>'+"/"+id);
                        modal.find('.modal-body .preview-content').attr('src', thumbnail);
                    })
                </script>

                <script>
                    const handleChange = () => {
                        const fileUploader = document.querySelector('#input-file');
                        const fileUploaderEdit = document.querySelector('#input-file-edit');
                        const getFile = fileUploader.files
                        const getFileEdit = fileUploaderEdit.files
                        if (getFile.length !== 0) {
                            const uploadedFile = getFile[0];
                            readFile(uploadedFile);
                        }
                        if (getFileEdit.length !== 0) {
                            const uploadedFile = getFileEdit[0];
                            readFile(uploadedFile);
                        }
                    }

                    const readFile = (uploadedFile) => {
                        if (uploadedFile) {
                            const reader = new FileReader();
                            reader.onload = () => {
                            const parent = document.querySelector('.preview-box');
                            const parent2 = document.querySelector('.preview-box2');
                            parent.innerHTML = `<img class="preview-content img-fluid" src=${reader.result} />`;
                            parent2.innerHTML = `<img class="preview-content img-fluid" src=${reader.result} />`;
                            };
                            reader.readAsDataURL(uploadedFile);
                        }
                    }
                </script>
<?= $this->endSection() ?>