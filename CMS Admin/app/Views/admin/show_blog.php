<?= $this->extend('layout/page_layout') ?>

<?= $this->section('content') ?>
                <div class="container-fluid">
                    <div class="layout-specing">
                        <div class="d-md-flex justify-content-between">
                            <div>
                                <h5 class="mb-0"><?=$artikel->title;?></h5>

                                <nav aria-label="breadcrumb" class="d-inline-block mt-1">
                                    <ul class="breadcrumb breadcrumb-muted bg-transparent rounded mb-0 p-0">
                                        <li class="breadcrumb-item text-capitalize"><a href=<?= base_url("/admin/")?>>Home</a></li>
                                        <li class="breadcrumb-item text-capitalize"><a href=<?= base_url("/admin/blog")?>>Blogs</a></li>
                                        <li class="breadcrumb-item text-capitalize active" aria-current="page"><?=$artikel->title;?></li>
                                    </ul>
                                </nav>
                            </div>

                            <div class="mt-4 mt-sm-0">
                                <a href="#" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#editblog">Edit Blog</a>
                            </div>
                        </div>
                    
                        <div class="row">
                        <div class="col-lg-12 col-md-6 mt-4">
                                <div class="card blog blog-primary blog-detail border-0 shadow rounded">
                                    <?php
                                    if (!empty($artikel->thumbnail)) {
                                        echo '<img src="' . base_url("assets/images/$artikel->thumbnail") . '" class="img-fluid rounded-top" alt="...">';
                                    } else {
                                        echo '<img src="' . base_url("assets/images/blog/no-image.jpg") . '" class="img-fluid rounded-top" alt="...">';
                                    }
                                    ?>
                                    <div class="card-body content">
                                        <h4 class="mt-3"><?=$artikel->title;?></h4>
                                        <p class="mt-3"><?=$artikel->content;?></p>
                                    </div>
                                </div>
                            </div>
                        </div><!--end row-->
                    </div>
                </div><!--end container-->

                <!-- Start Modal Edit -->
                <div class="modal fade" id="editblog" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header border-bottom p-3">
                                <h5 class="modal-title" id="exampleModalLabel">Edit Blog</h5>
                                <button type="button" class="btn btn-icon btn-close" data-bs-dismiss="modal" id="close-modal"><i class="uil uil-times fs-4 text-dark"></i></button>
                            </div>
                            <div class="modal-body p-3 pt-4">
                                <div class="row">
                                    <form method="post" action=<?=base_url("/blog/edit")?> enctype="multipart/form-data">
                                    <div class="col-md-12 mt-4 mt-sm-0">
                                        <div>
                                            <!-- <form method="post" action="/blog/simpan"> -->
                                                <div class="row">

                                                    <div class="col-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Blog Title <span class="text-danger">*</span></label>
                                                            <input name="title" id="blogtitle" type="text" class="form-control" placeholder="Title :" value="<?=$artikel->title;?>">
                                                        </div>
                                                    </div><!--end col-->
            
                                                    <div class="col-lg-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Content <span class="text-danger">*</span></label>
                                                            <textarea name="content" class="summernote form-control" id="summernote-edit" rows="4" placeholder="Content :"><?=$artikel->content;?></textarea>
                                                        </div>
                                                    </div><!--end col-->

                                                    <div class="col-lg-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Meta Description <span class="text-danger">*</span></label>
                                                            <textarea name="meta-description" class="form-control" rows="4" placeholder="For SEO Purpose, write the first pharagraph of content above here :" required><?=$artikel->meta_description;?></textarea>
                                                        </div>
                                                    </div>
                                                    <!--end col-->

                                                    <div class="col-12">
                                                        <div class="mb-3">
                                                            <p class="text-muted">Upload your blog thumbnail here, Please click "Upload Image" Button.</p>
                                                            <div class="preview-box d-block justify-content-center rounded shadow overflow-hidden bg-light p-1">
                                                                <?php
                                                                if (!empty($artikel->thumbnail)) {
                                                                    echo '<img src="' . base_url("assets/images/$artikel->thumbnail") . '" class="preview-content img-fluid" alt="...">';
                                                                } else {
                                                                    echo '<img src="' . base_url("assets/images/blog/no-image.jpg") . '" class="preview-content img-fluid" alt="...">';
                                                                }
                                                                ?>
                                                            </div>
                                                            <input type="file" id="input-file-edit" name="file_upload_edit" accept="image/*" onchange={handleChange()} hidden />
                                                            <label class="btn-upload btn btn-primary mt-4" for="input-file-edit">Upload Image</label>
                                                        </div>
                                                    </div><!--end col-->
            
                                                    <div class="col-lg-12 text-end">
                                                        <!-- <?php $session = session()?>
                                                        <input name="author" id="author" type="hidden" value="<?=$session->get('username');?>"> -->
                                                        <input name="id" id="blogid" type="hidden" value="<?=$artikel->id;?>">
                                                        <a href=<?= base_url("/blog/hapus/".$artikel->id)?> class="button-delete btn btn-danger">Delete Blog</a>
                                                        <button type="submit" class="btn btn-primary">Edit Blog</button>
                                                    </div><!--end col-->
                                                </div>
                                            <!-- </form> -->
                                        </div>
                                    </div><!--end col-->
                                    </form>
                                </div><!--end row-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End modal -->

                <style>
                    .note-editable {
                        background-color: #FFFFFF !important;
                    }
                    .card-img-top {
                        width: 100%;
                        height: 15vw;
                        object-fit: cover;
                    }
                </style>

                <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
                <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
                <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

                <script>
                    $(document).ready(function() {
                        $('.summernote').summernote({
                            callbacks: {
                                onImageUpload: function(files) {
                                    for (let i = 0; i < files.length; i++) {
                                        $.upload(files[i]);
                                    }
                                },
                                onMediaDelete: function(target) {
                                    $.delete(target[0].src);
                                }
                            },
                            height: 200,
                            toolbar: [
                                ["style", ["bold", "italic", "underline", "clear"]],
                                ["fontname", ["fontname"]],
                                ["fontsize", ["fontsize"]],
                                ["color", ["color"]],
                                ["para", ["ul", "ol", "paragraph"]],
                                ["height", ["height"]],
                                ["insert", ["link", "picture", "imageList", "video", "fullscreen"]],

                            ],
                            imageList: {
                                endpoint: "<?php echo site_url('berita/listGambar') ?>",
                                fullUrlPrefix: "<?php echo base_url('uploads/berkas') ?>/",
                                thumbUrlPrefix: "<?php echo base_url('uploads/berkas') ?>/"
                            }
                        });

                        $.upload = function(file) {
                            let out = new FormData();
                            out.append('file', file, file.name);
                            $.ajax({
                                method: 'POST',
                                url: '<?php echo site_url('summernote/upload_image') ?>',
                                contentType: false,
                                cache: false,
                                processData: false,
                                data: out,
                                success: function(img) {
                                    $('.summernote').summernote('insertImage', img);
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                    console.error(textStatus + " " + errorThrown);
                                }
                            });
                        };
                        $.delete = function(src) {
                            $.ajax({
                                method: 'POST',
                                url: '<?php echo site_url('summernote/delete_image') ?>',
                                cache: false,
                                data: {
                                    src: src
                                },
                                success: function(response) {
                                    console.log(response);
                                }

                            });
                        };
                    });

                    function konfirmasi(url) {
                        var result = confirm("Want to delete?");
                        if (result) {
                            window.location.href = url;
                        }
                    }
                </script>

                <script>
                    const handleChange = () => {
                        const fileUploader = document.querySelector('#input-file-edit');
                        
                        const getFile = fileUploader.files
                        
                        if (getFile.length !== 0) {
                            const uploadedFile = getFile[0];
                            readFile(uploadedFile);
                        }
                
                    }

                    const readFile = (uploadedFile) => {
                        if (uploadedFile) {
                            const reader = new FileReader();
                            reader.onload = () => {
                            const parent = document.querySelector('.preview-box');
                            parent.innerHTML = `<img class="preview-content img-fluid" src=${reader.result} />`;
                            };
                            reader.readAsDataURL(uploadedFile);
                        }
                    }
                </script>
<?= $this->endSection() ?>